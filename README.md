# Building on Windows
<ol>
<li>Clone the repository</li>
<li>Import the .sln into visual studio and build.</li>
</ol>
# Building on Liux
Move to a folder where you want the project to be downloaded
'''
git clone https://scm.ti-edu.ch/repogit/labingsw022020202101g3d
cd labingsw022020202101g3d
cmake .
cmake --build .
'''

### Disclaimer
if you used a build directory other than "." you have to move the assets directory to the build directory. The program <b>must</b> execute in the same folder of 'assets' folder.

After this, program is build into cmake-build-debug directory.
You can cd into that directory and then run the file 'CGProject'.
