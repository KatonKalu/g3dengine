//
// Created by kalu on 12/17/20.
//

#include "MaterialManager.h"

MaterialManager &MaterialManager::getInstance() {
    static MaterialManager instance{};
    return instance;
}

void MaterialManager::addMaterial(std::string name, Material *material) {
    materialMap.emplace(name, material);
}

void MaterialManager::removeMaterial(std::string name) {
    materialMap.erase(name);
}

Material *MaterialManager::getMaterial(std::string name) {
    return materialMap.at(name);
}
