#include "FileParser.h"
#include "cglib.h"


FileParser::FileParser(ParsingStrategy* strategy): m_strategy(strategy)
{
}

FileParser::~FileParser()
{
	if (m_strategy != nullptr) delete m_strategy;
}

void FileParser::setStrategy(ParsingStrategy* strategy)
{
	if (m_strategy != nullptr) delete m_strategy;
	this->m_strategy = strategy;
}

SceneNode * FileParser::loadFromFile(const std::string &fileName, bool assetsDirRelative)
{
    std::string finalPath = fileName;
    if(assetsDirRelative){
        std::string assetsDir{CGLib::getAssetsDirPath()};
        finalPath = assetsDir + separator() + fileName;
    }

	FILE* in = fopen(finalPath.c_str(), "rb");
    m_strategy->setInputFile(in);
    m_strategy->execute();
	return m_strategy->getResult();
}
