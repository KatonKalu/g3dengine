//
// Created by ubuntu on 22/12/20.
//

#include <TextureManager.h>
#include <TrilinearFilteringStrategy.h>
#include <AnisotropicFilteringStrategy.h>
#include <BilinearFilteringStrategy.h>
#include <NoFilteringStrategy.h>

TextureManager &TextureManager::getInstance() {

    static TextureManager instance{};

    return instance;

}

void TextureManager::forceMassiveReload() const {
    for (std::pair<int, Texture *> element : m_texture_map)
    {
        element.second->clean();
        element.second->loadToGPU();
    }
}

void TextureManager::addTexture(unsigned int id, Texture *texture) {
    m_texture_map.emplace(id, texture);
}

void TextureManager::removeTexture(unsigned int id) {
    m_texture_map.erase(id);
}

Texture *TextureManager::getTexture(unsigned int id) const {
    return m_texture_map.at(id);
}

void TextureManager::setFilteringTemplate(FilteringTemplate *filteringTemplate) {
    m_filtering = filteringTemplate;
}

FilteringTemplate* TextureManager::getFilteringTemplate() const {
    return m_filtering;
}

TextureManager::TextureManager() {
    this->m_filtering = new FilteringTemplate();
    m_filtering->setPerPixelStrategy(new NoFilteringStrategy());
}
