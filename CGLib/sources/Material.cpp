//
// Created by purpleknight on 11/16/20.
//

#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>
#include "SceneObject.h"
#include "LuminousObject.h"
#include "Material.h"

Material::Material(const glm::vec4 &diffuse, const glm::vec4 &ambient, const glm::vec4 &specular,
                   const glm::vec4 &emission,
                   float shine, unsigned int orientation)
        : LuminousObject(diffuse, ambient, specular, emission),
          m_shine(shine),
          m_orientation(orientation) {};

float Material::getShine() const {
    return m_shine;
}

void Material::setShine(float pShine) {
    Material::m_shine = pShine;
}

unsigned int Material::getOrientation() const {
    return m_orientation;
}

void Material::setOrientation(unsigned int orientation) {
    Material::m_orientation = orientation;
}

void Material::apply() {

    glMaterialf(m_orientation, CGLIB_SHININESS, powf(2.0f, getShine()));
    glMaterialfv(m_orientation, CGLIB_AMBIENT, glm::value_ptr(getAmbient()));
    glMaterialfv(m_orientation, CGLIB_DIFFUSE, glm::value_ptr(getDiffuse()));
    glMaterialfv(m_orientation, CGLIB_SPECULAR, glm::value_ptr(getSpecular()));
    glMaterialfv(m_orientation, CGLIB_EMISSION, glm::value_ptr(getEmission()));
}

Texture *Material::getTexture() const {
    return m_texture;
}

void Material::setTexture(Texture *mTexture) {
    m_texture = mTexture;
}
