//
// Created by kalu on 12/25/20.
//

#include "FilteringTemplate.h"

void FilteringTemplate::setPerPixelStrategy(IStrategy *strategy) {
    this->m_pixelStrategy = strategy;
}

void FilteringTemplate::setAnisotropicStrategy(IStrategy *strategy) {
    this->m_anisotropicStrategy = strategy;
}

void FilteringTemplate::execute() {
    m_pixelStrategy->execute();

    if(m_anisotropicStrategy != nullptr)
        m_anisotropicStrategy->execute();
}

FilteringTemplate::~FilteringTemplate() {
    delete this->m_anisotropicStrategy;
    delete this->m_pixelStrategy;
}

void FilteringTemplate::disableAnisotropicStrategy() {
    if(this->m_anisotropicStrategy) {
        m_anisotropicStrategy = nullptr;
    }
}

IStrategy* FilteringTemplate::getPerPixelStrategy() const {
    return m_pixelStrategy;
}

IStrategy* FilteringTemplate::getAnisotropicStrategy() const {
    return m_anisotropicStrategy;
}
