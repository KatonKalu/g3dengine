//
// Created by kalu on 12/25/20.
//

#ifndef CGLIB_PARSINGSTRATEGY_CPP
#define CGLIB_PARSINGSTRATEGY_CPP

#include "ParsingStrategy.h"



#endif //CGLIB_PARSINGSTRATEGY_CPP

void ParsingStrategy::setInputFile(FILE *fp){
    this->m_fp = fp;
}

SceneNode *ParsingStrategy::getResult() const {
    return this->m_parsed;
}
