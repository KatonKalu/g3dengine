//
// Created by kalu on 22/10/20.
//

#include <vector>
#include <glm/glm.hpp>
#include "SceneObject.h"

#include "SceneNode.h"

SceneNode::SceneNode(const glm::vec3 &rotate,
                     const glm::vec3 &translate,
                     const glm::vec3 &scale)
        : SceneObject(rotate, translate, scale), m_children(), m_parent(nullptr) {}

void SceneNode::addChild(SceneNode *n) {
    n->m_parent = this;
    this->m_children.push_back(n);
}

SceneNode *SceneNode::getChild(int index) {
    return this->m_children.at(index);
}

std::vector<SceneNode *> SceneNode::getChildren() {
    return this->m_children;
}

SceneNode *SceneNode::getParent() const {
    return m_parent;
}

glm::mat4 SceneNode::getTotalTransform() const {
    glm::mat4 parentTransform;
    if (this->m_parent == nullptr) {
        parentTransform = glm::mat4(1.0f);
    } else {
        parentTransform = m_parent->getTotalTransform();
    }

    return parentTransform * getLocalTransform() * m_baseTransform;
}

SceneNode* SceneNode::findNodeInChildren(std::string nodeId) const {
    if(m_id == nodeId) return const_cast<SceneNode *>(this);

    for(auto node: m_children){
        SceneNode* found = node->findNodeInChildren(nodeId);
        if(found) return found;
    }

    return nullptr;
}

void SceneNode::removeChildren(const std::string& nodeId) {
    for(int i=0; i<m_children.size(); i++){
        if(m_children.at(i)->getId() == nodeId)
            m_children.erase(m_children.begin() + i);
    }
}





