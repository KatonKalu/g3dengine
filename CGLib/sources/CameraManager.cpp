//
// Created by purpleknight on 12/22/20.
//

#include <algorithm>
#include "CameraManager.h"

CameraManager::CameraManager()
        : m_cameras(std::vector<Camera *>{}) {
    m_active_camera = nullptr;
    m_default_camera = new Camera{};
}

const std::vector<Camera *> &CameraManager::getCameras() const {
    return m_cameras;
}

void CameraManager::add(Camera *camera) {
    m_cameras.push_back(camera);
}

void CameraManager::remove(Camera *camera) {
    auto toRemove = std::find_if(m_cameras.begin(),
                                 m_cameras.end(),
                                 [&camera](const Camera *next) {
                                     return (camera == next);
                                 });

    m_cameras.erase(toRemove);
}


Camera *CameraManager::getActiveCamera() {
    if (m_active_camera == nullptr)
        return m_default_camera;
    return m_active_camera;
}

int CameraManager::enable(Camera *camera) {
    if (!isPresent(camera) || camera == nullptr)
        return 0;

    for (auto &m_camera : m_cameras) {
        if (m_camera == camera) {
            m_active_camera = m_camera;
        }
    }
    return 1;
}


int CameraManager::disable() {
    if (m_active_camera == nullptr)
        return 0;

    m_active_camera = nullptr;
    return 1;
}


bool CameraManager::isPresent(Camera *camera) {
    for (auto & m_camera : m_cameras) {
        if (m_camera == camera) {
            return true;
        }
    }

    return false;
}

int CameraManager::size() {
    return m_cameras.size();
}