//
// Created by purpleknight on 11/16/20.
//

#include <glm/glm.hpp>
#include "SceneNode.h"
#include "LuminousObject.h"
#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include "cglib.h"

#include "Light.h"

void Light::apply() {

    glLightfv(getLightRef(), CGLIB_POSITION, glm::value_ptr(getPosition()));

    glLightfv(m_light_ref, CGLIB_AMBIENT, glm::value_ptr(getAmbient()));
    glLightfv(m_light_ref, CGLIB_DIFFUSE, glm::value_ptr(getDiffuse()));
    glLightfv(m_light_ref, CGLIB_SPECULAR, glm::value_ptr(getSpecular()));

    glLightf(m_light_ref, CGLIB_ATTENUATION_CONSTANT, m_constant);
    glLightf(m_light_ref, CGLIB_ATTENUATION_LINEAR, m_linear);
    glLightf(m_light_ref, CGLIB_ATTENUATION_QUADRATIC, m_quadratic);

}

glm::vec4 Light::getPosition() const {
    glm::vec4 vec{0.0f, 0.0f, 1.0f, 1.0f};
    vec.w = m_position;
    return vec;
}

void Light::setPosition(const float position) {
    Light::m_position = position;
}

float Light::getLinearAttenuation() const {
    return m_linear;
}

void Light::setLinearAttenuation(float linear) {
    Light::m_linear = linear;
}

float Light::getConstantAttenuation() const {
    return m_constant;
}

void Light::setConstantAttenuation(float constant) {
    Light::m_constant = constant;
}

float Light::getQuadraticAttenuation() const {
    return m_quadratic;
}

void Light::setQuadraticAttenuation(float quadratic) {
    Light::m_quadratic = quadratic;
}

void Light::setInfluence(float radius){
    this->setConstantAttenuation(1.0f);
    radius = radius * 10;
    this->setQuadraticAttenuation(1.0f / radius);
    this->setLinearAttenuation(-getQuadraticAttenuation() * radius / 2.0f - 1.0f / radius);
}

Light::Light(unsigned int light_ref,
             const glm::vec3 &rotate,
             const glm::vec3 &translate,
             const glm::vec3 &scale,
             const glm::vec4 &diffuse,
             const glm::vec4 &ambient,
             const glm::vec4 &specular,
             float position,
             float linear,
             float constant,
             float quadratic)
        : SceneNode(rotate, translate, scale),
          LuminousObject(diffuse, ambient, specular),
          m_light_ref(light_ref),
          m_position(position),
          m_linear(linear),
          m_constant(constant),
          m_quadratic(quadratic) {
    setId(getId().append("light"));
}


unsigned int Light::getLightRef() const {
    return m_light_ref;
}

void Light::enable() const{
    glEnable(m_light_ref);
}

void Light::disable() const{
    glDisable(m_light_ref);
}

void Light::render(glm::mat4 cameraMatrix) {
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(glm::value_ptr(cameraMatrix * getTotalTransform()));

    if(getLightRef() != 0)
        apply();

//    SceneNode::render(cameraMatrix);
}
