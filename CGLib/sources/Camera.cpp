//
// Created by purpleknight on 11/16/20.
//

#include "SceneNode.h"
#include "Camera.h"
#include <glm/glm.hpp>


Camera::Camera(const glm::vec3 &rotate,
               const glm::vec3 &translate,
               const glm::vec3 &scale,
               const glm::vec3 &eye,
               const glm::vec3 &center,
               const glm::vec3 &up)
        : SceneNode(rotate, translate, scale), m_eye(eye),
          m_center(center), m_up(up) {
}


glm::mat4 Camera::getCamera() const {
    return getCameraMatrix(m_eye, m_center, m_up);
}

const glm::vec3 &Camera::getEye() const {
    return m_eye;
}

const glm::vec3 &Camera::getCenter() const {
    return m_center;
}

const glm::vec3 &Camera::getUp() const {
    return m_up;
}

void Camera::setEye(const glm::vec3 &mEye) {
    m_eye = mEye;
}

void Camera::setCenter(const glm::vec3 &mCenter) {
    m_center = mCenter;
}

void Camera::setUp(const glm::vec3 &mUp) {
    m_up = mUp;
}

void Camera::rotate(glm::vec3 eulerAngles) {
    glm::vec4 m_up_vec = getRotationMatrix(eulerAngles) * glm::vec4{m_up.x, m_up.y, m_up.z, 1.0f};

    glm::vec3 m_center_direction = glm::normalize(m_center - m_eye);
    auto m_center_direction_calculated = getRotationMatrix(eulerAngles) *
                                         glm::vec4{m_center_direction.x, m_center_direction.y, m_center_direction.z, 1};


    m_up = glm::normalize(glm::vec3{m_up_vec.x, m_up_vec.y, m_up_vec.z});
    m_center = m_eye + glm::vec3{m_center_direction_calculated.x, m_center_direction_calculated.y,
                                 m_center_direction_calculated.z};

}


void Camera::translate(glm::vec3 translation) {
    glm::vec4 m_eye_vec = getTranslationMatrix(translation) * glm::vec4{m_eye.x, m_eye.y, m_eye.z, 1.0f};
    glm::vec4 m_center_vec = getTranslationMatrix(translation) * glm::vec4{m_center.x, m_center.y, m_center.z, 1.0f};

    m_eye = glm::vec3{m_eye_vec.x, m_eye_vec.y, m_eye_vec.z};
    m_center = glm::vec3{m_center_vec.x, m_center_vec.y, m_center_vec.z};
}


void Camera::lookTo(glm::vec3 direction, glm::vec3 up) {
    m_center = m_eye + direction;
    m_up = up;
}

void Camera::render(glm::mat4 cameraMatrix) {}

