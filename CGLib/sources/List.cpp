//
// Created by purpleknight on 12/6/20.
//

#include "List.h"

void List::pass(SceneNode *node) {
    add(node);

    std::vector<SceneNode*> children = node->getChildren();
    if (!children.empty())
        for (auto &element : children) {
            pass(element);
        }
}

void List::add(SceneNode *node) {
    if (node->getId().find("light")) {
        elements.push_front(node);
    } else if (node->getId().find("mesh")) {
        elements.push_back(node);
    }
}

void List::render(glm::mat4 cameraMatrix) {
    for (auto &element : elements) {
        element->render(cameraMatrix);
    }
}
