//
// Created by ubuntu on 21/12/20.
//

#include "Texture.h"
#include <GL/freeglut.h>
#include <TextureManager.h>

void Texture::loadToGPU() {

    glGenTextures(1, &m_tex_id);
    glBindTexture(GL_TEXTURE_2D, m_tex_id);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    TextureManager::getInstance().getFilteringTemplate()->execute();

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, m_bitmap);
    glBindTexture(GL_TEXTURE_2D, 0);
}


const unsigned int Texture::getTexId() const {
    return m_tex_id;
}

unsigned char * Texture::getBitmap() const {
    return m_bitmap;
}

void Texture::setBitmap(unsigned char *mBitmap) {
    m_bitmap = mBitmap;
}

unsigned int Texture::getWidth() const {
    return m_width;
}

void Texture::setWidth(unsigned int mWidth) {
    m_width = mWidth;
}

unsigned int Texture::getHeight() const {
    return m_height;
}

void Texture::setHeight(unsigned int mHeight) {
    m_height = mHeight;
}

void Texture::clean() {
    glDeleteTextures(1, &m_tex_id);
}

Texture::~Texture() {
    clean();
}




