#include <GL/freeglut.h>
#include <glm/gtc/type_ptr.hpp>
#include <utility>
#include <string>
#include "transform.h"

#include "cglib.h"

/**
* Wrapped FreeGLUT DisplayMode options
*/
unsigned int CGLIB_RGB = GLUT_RGB;
unsigned int CGLIB_RGBA = GLUT_RGBA;
unsigned int CGLIB_SINGLE_BUFFER = GLUT_SINGLE;
unsigned int CGLIB_DOUBLE_BUFFER = GLUT_DOUBLE;
unsigned int CGLIB_DEPTH_BUFFER = GLUT_DEPTH;

/**
* Wrapped OpenGL options
*/
const unsigned int CGLIB_CLEAR_COLOR = GL_COLOR_BUFFER_BIT;
const unsigned int CGLIB_CLEAR_DEPTH = GL_DEPTH_BUFFER_BIT;
const unsigned int CGLIB_DEPTH = GL_DEPTH_TEST;
const unsigned int CGLIB_LIGHTING = GL_LIGHTING;
const unsigned int CGLIB_MMODE_MODELVIEW = GL_MODELVIEW;
const unsigned int CGLIB_MMODE_PROJECTION = GL_PROJECTION;
const unsigned int CGLIB_CULLING = GL_CULL_FACE;
const unsigned int CGLIB_WIREFRAME = GL_LINE;
const unsigned int CGLIB_SOLID = GL_FILL;
const unsigned int CGLIB_BOTH = GL_FRONT_AND_BACK;
const unsigned int CGLIB_FRONT = GL_FRONT;
const unsigned int CGLIB_BACK = GL_BACK;
const unsigned int CGLIB_POSITION = GL_POSITION;
const unsigned int CGLIB_SPOT_CUTOFF = GL_SPOT_CUTOFF;
const unsigned int CGLIB_SPOT_DIRECTION = GL_SPOT_DIRECTION;
const unsigned int CGLIB_ATTENUATION_CONSTANT = GL_CONSTANT_ATTENUATION;
const unsigned int CGLIB_ATTENUATION_LINEAR = GL_LINEAR_ATTENUATION;
const unsigned int CGLIB_ATTENUATION_QUADRATIC = GL_QUADRATIC_ATTENUATION;
const unsigned int CGLIB_AMBIENT = GL_AMBIENT;
const unsigned int CGLIB_DIFFUSE = GL_DIFFUSE;
const unsigned int CGLIB_SPECULAR = GL_SPECULAR;
const unsigned int CGLIB_EMISSION = GL_EMISSION;
const unsigned int CGLIB_SHININESS = GL_SHININESS;
const unsigned int CGLIB_LIGHT0 = GL_LIGHT0;

struct context_status {
    const char *contextName;
    const char *assetsDir;
    unsigned int displayMode;
    bool isDepthBufferUsed;
    int windowId;
    int windowWidth;
    int windowHeight;
    int windowX;
    int windowY;
    int fps;
    bool showFps;
    void (*display_callback)();
    void (*reshape_callback)(int, int);
} typedef CTX_STATUS;

CTX_STATUS *contextStatus = static_cast<CTX_STATUS *>(malloc(sizeof(CTX_STATUS)));;

void updateFPS();
void templateDisplayCallback();
void templateReshapeCallback(int width, int height);
void renderFps();

void CGLib::init(const char *contextName, int *pargc, char **argv) {
    contextStatus->contextName = contextName;
    contextStatus->showFps = false;
    glutInit(pargc, argv);
}

void CGLib::setWindowOptions(unsigned int displayMode, int windowW, int windowH, int windowX, int windowY) {
    contextStatus->displayMode = displayMode;
    contextStatus->windowWidth = windowW;
    contextStatus->windowHeight = windowH;
    contextStatus->windowX = windowX;
    contextStatus->windowY = windowY;
    if ((displayMode & CGLIB_DEPTH_BUFFER) == CGLIB_DEPTH_BUFFER)
        contextStatus->isDepthBufferUsed = true;

    glutInitDisplayMode(contextStatus->displayMode);
    glutInitWindowSize(contextStatus->windowWidth, contextStatus->windowHeight);
    glutInitWindowPosition(contextStatus->windowX, contextStatus->windowY);
}

void CGLib::createWindow() {
    contextStatus->windowId = glutCreateWindow(contextStatus->contextName);
    CGLib::enableLights();
    CGLib::enableFaceCulling();
    CGLib::enableDepthTest();
    CGLib::enableSmoothShading();
}

void CGLib::setBackgroundColor(float r, float g, float b, float a) {
    glClearColor(r, g, b, a);
}

void CGLib::clear() {

    unsigned int toClear =
            CGLIB_CLEAR_COLOR | ((contextStatus->isDepthBufferUsed) ? CGLIB_CLEAR_DEPTH : 0x0000);

    glClear(toClear);

}

void CGLib::setDisplayCallback(void (*callback)()) {
    contextStatus->display_callback = callback;
    glutDisplayFunc(templateDisplayCallback);
}

void CGLib::setWindowReshapeCallback(void (*callback)(int, int)) {
    contextStatus->reshape_callback = callback;
    glutReshapeFunc(templateReshapeCallback);
}

void CGLib::setKeyboardEventCallback(void (*callback)(unsigned char key, int mouseX, int mouseY)) {

    glutKeyboardFunc(callback);
}

void CGLib::updateProjectionMatrix(int width, int height) {
    glViewport(contextStatus->windowX, contextStatus->windowY, width, height);
    glMatrixMode(CGLIB_MMODE_PROJECTION);
    glLoadMatrixf(glm::value_ptr(getProjectionMatrix(45.0f, (float) width / (float) height, 0.5f, 100000.0f)));
}

void CGLib::disableLights() {
    glDisable(CGLIB_LIGHTING);
    glDisable(GL_NORMALIZE);
}

void CGLib::enableLights() {
    glEnable(CGLIB_LIGHTING);
    glEnable(GL_NORMALIZE);
}

void CGLib::enableSmoothShading() {
    glShadeModel(GL_SMOOTH);
}

void CGLib::enableFlatShading() {
    glShadeModel(GL_FLAT);
}

void CGLib::enableFpsCounter() {
    contextStatus->showFps = true;
}

void CGLib::disableFpsCounter() {
    contextStatus->showFps = false;
}

void CGLib::refreshDisplay() {
    glutSwapBuffers();
    glutPostWindowRedisplay(contextStatus->windowId);
}

void CGLib::enterMainLoop() {

    glutMainLoop();
}

void CGLib::enableDepthTest() {
    glEnable(CGLIB_DEPTH);
}

void CGLib::disableDepthTest() {
    glDisable(CGLIB_DEPTH);
}

void CGLib::enableFaceCulling() {
    glEnable(CGLIB_CULLING);
}

void CGLib::disableFaceCulling() {
    glDisable(CGLIB_CULLING);
}

void CGLib::setPolygonMode(unsigned int face, unsigned int mode) {
    glPolygonMode(face, mode);
}

void CGLib::setAssetsDirPath(const char* dirPath){
    contextStatus->assetsDir = dirPath;
}

const char* CGLib::getAssetsDirPath() {
    return contextStatus->assetsDir;
}

void CGLib::setMouseCallback(void (*callback)(int, int)) {
    glutPassiveMotionFunc(callback);
}

void updateFPS(){

    static float frames = 0.0f;
    static int lastUpdateTime = 0;
    int currentTime = glutGet(GLUT_ELAPSED_TIME);

    frames++;

    if(currentTime - lastUpdateTime > 1000){

        contextStatus->fps = (int) (frames * 1000.0f) / (currentTime - lastUpdateTime);
        lastUpdateTime = currentTime;
        frames = 0;

    }

}

void templateDisplayCallback(){

    CGLib::clear();
    updateFPS();

    contextStatus->display_callback();
    if(contextStatus->showFps) {
        renderFps();
    }

    CGLib::refreshDisplay();
}

void templateReshapeCallback(int width, int height){

    contextStatus->windowWidth = width;
    contextStatus->windowHeight = height;

    CGLib::updateProjectionMatrix(width, height);

    contextStatus->reshape_callback(width, height);

}

void renderFps(){

    glm::mat4 ortho = glm::ortho(0.0f,
                                 (float)contextStatus->windowWidth,
                                 0.0f,
                                 (float)contextStatus->windowHeight,
                                 -1.0f,
                                 1.0f);

    CGLib::disableLights();

    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(glm::value_ptr(ortho));

    glColor3f(1.0f, 1.0f, 1.0f);

    glRasterPos2f(10.0f, 10.0f);
    glutBitmapString(GLUT_BITMAP_8_BY_13, (unsigned char*) (std::to_string(contextStatus->fps) + " FPS").c_str());

    glPopMatrix();
    CGLib::updateProjectionMatrix(contextStatus->windowWidth, contextStatus->windowHeight);

    CGLib::enableLights();
}