//
// Created by ubuntu on 23/11/20.
//
#include <glm/glm.hpp>
#include "Light.h"
#include "DirectionalLight.h"

void DirectionalLight::apply() {

    Light::apply();

}

DirectionalLight::DirectionalLight(unsigned int light_ref,
                                   const glm::vec3 &rotate,
                                   const glm::vec3 &translate,
                                   const glm::vec3 &scale,
                                   const glm::vec4 &diffuse,
                                   const glm::vec4 &ambient,
                                   const glm::vec4 &specular,
                                   float linear,
                                   float constant,
                                   float quadratic)
        : Light(light_ref, rotate, translate, scale, diffuse, ambient, specular, 0.0f, linear, constant, quadratic) {}

glm::vec4 DirectionalLight::getPosition() const {
    glm::vec4 vec{0.0f, 0.0f, 1.0f, 1.0f};
    vec = getTotalTransform() * vec;
    vec.w = m_position;
    return vec;
}
