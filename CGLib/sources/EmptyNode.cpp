//
// Created by kalu on 12/3/20.
//

#include <SceneNode.h>
#include <EmptyNode.h>

EmptyNode::EmptyNode(const glm::vec3 &rotate,
                     const glm::vec3 &translate,
                     const glm::vec3 &scale) : SceneNode(rotate, translate, scale) {
}

void EmptyNode::render(glm::mat4 cameraMatrix) {

}


