
#include <List.h>
#include "SceneGraph.h"
#include "Camera.h"

SceneGraph& SceneGraph::getInstance(){
  static SceneGraph instance{};

  return instance;
}

void SceneGraph::renderScene(Camera *camera) const {

    List list{};
    for (auto node : m_rootNodes) {
        list.pass(node);
    }
    list.render(camera->getCamera());
}

void SceneGraph::addRootNode(SceneNode *rootNode) {
    m_rootNodes.push_back(rootNode);
}

void SceneGraph::setRootNodes(std::vector<SceneNode *> &newRootNodes) {
    this->m_rootNodes = newRootNodes;
}

std::vector<SceneNode *> SceneGraph::getRootNodes() const {
    return this->m_rootNodes;
}

SceneNode* SceneGraph::findNode(std::string nodeId) const {
    for (auto node : m_rootNodes){
        SceneNode* found = node->findNodeInChildren(nodeId);
        if(found) return found;
    }

    return nullptr;
}

SceneNode* SceneGraph::popNode(const std::string& nodeId) const {
    SceneNode* node = findNode(nodeId);
    node->getParent()->removeChildren(nodeId);
    return node;
}