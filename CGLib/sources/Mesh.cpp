/* TODO:
 *   - light and camera manager
 */

#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Material.h"
#include "Mesh.h"

const unsigned int * Mesh::getTriangles() const {
    return m_triangles;
}

const glm::vec3 * Mesh::getVertices() const {
    return m_vertices;
}

const glm::vec3 *Mesh::getVertexInTriangle(int triangleIndex, int vertexIndex) const {
    return &m_vertices[m_triangles[triangleIndex*3 + vertexIndex]];
}

void Mesh::render(glm::mat4 camera) {
    glMatrixMode(GL_MODELVIEW);

    glLoadMatrixf(glm::value_ptr(camera * getTotalTransform()));

    m_material->apply();
    renderMesh();
}

void Mesh::renderMesh() const {

    if (m_material->getTexture() != nullptr) {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, m_material->getTexture()->getTexId());
    } else {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    glBegin(GL_TRIANGLES);

    for (int i = 0; i < this->getFacesNumber(); i++) { // for each triangle

        const glm::vec3* a = this->getVertexInTriangle(i,0);
        const glm::vec3* b = this->getVertexInTriangle(i,1);
        const glm::vec3* c = this->getVertexInTriangle(i,2);

        unsigned int v1 = m_triangles[i * 3];
        unsigned int v2 = m_triangles[(i * 3) + 1];
        unsigned int v3 = m_triangles[(i * 3) + 2];

        glNormal3fv(glm::value_ptr(m_normals[v1]));
        glTexCoord2f(m_mapping[v1].x, m_mapping[v1].y);
        glVertex3fv(glm::value_ptr(*a)); // triangle vert1

        glNormal3fv(glm::value_ptr(m_normals[v2]));
        glTexCoord2f(m_mapping[v2].x, m_mapping[v2].y);
        glVertex3fv(glm::value_ptr(*b)); // triangle vert2

        glNormal3fv(glm::value_ptr(m_normals[v3]));
        glTexCoord2f(m_mapping[v3].x, m_mapping[v3].y);
        glVertex3fv(glm::value_ptr(*c)); // triangle vert3

    }

    glDisable(GL_TEXTURE_2D);
    glEnd();
}


const Material *Mesh::getMaterial() const {
    return m_material;
}

void Mesh::setMaterial(Material *material) {
    Mesh::m_material = material;
}

const glm::vec2* Mesh::getMapping() const {
    return m_mapping;
}

void Mesh::setMapping(const glm::vec2* mMapping) {
    m_mapping = mMapping;
}

int Mesh::getVerticesNumber() const {
    return m_verticesNumber;
}

int Mesh::getFacesNumber() const {
    return m_trianglesNumber;
}

Mesh::~Mesh() {
    delete[] m_triangles;
    delete[] m_vertices;
    delete[] m_mapping;
}

void Mesh::setNormals(glm::vec3* normals) {
    this->m_normals = normals;
}