//
// Created by ubuntu on 29/11/20.
//

#include "SceneObject.h"

std::string SceneObject::generate_uuid_v4() {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> dis(0, 15);
    static std::uniform_int_distribution<> dis2(8, 11);

    std::stringstream ss;
    int i;
    ss << std::hex;
    for (i = 0; i < 8; i++) {
        ss << dis(gen);
    }
    ss << "-";
    for (i = 0; i < 4; i++) {
        ss << dis(gen);
    }
    ss << "-4";
    for (i = 0; i < 3; i++) {
        ss << dis(gen);
    }
    ss << "-";
    ss << dis2(gen);
    for (i = 0; i < 3; i++) {
        ss << dis(gen);
    }
    ss << "-";
    for (i = 0; i < 12; i++) {
        ss << dis(gen);
    };
    return ss.str();
}

void SceneObject::setId(const std::string &mId) {
    m_id = mId;
}

SceneObject::SceneObject(
        glm::vec3 rotate,
        glm::vec3 translate,
        glm::vec3 scale
) :
        m_id{generate_uuid_v4()},
        m_localRotation(getRotationMatrix(rotate)),
        m_localScale(getScaleMatrix(scale)),
        m_localTranslation(getTranslationMatrix(translate)) {
    updateLocalTransform();
}

std::string SceneObject::getId() const {
    return m_id;
}

glm::mat4 SceneObject::getRotation() const {
    return this->m_localRotation;
}

glm::mat4 SceneObject::getTranslation() const {
    return this->m_localTranslation;
}

glm::mat4 SceneObject::getScaling() const {
    return this->m_localScale;
}

glm::mat4 SceneObject::getLocalTransform() const {
    return m_localTransform;
}

void SceneObject::updateLocalTransform() {
    this->m_localTransform = this->m_localTranslation * this->m_localScale * this->m_localRotation;
}

void SceneObject::setLocalRotation(glm::mat4 rotation) {
    this->m_localRotation = rotation;
    updateLocalTransform();
}

void SceneObject::setLocalTranslation(glm::mat4 translation) {
    this->m_localTranslation = translation;
    updateLocalTransform();
}

void SceneObject::setLocalScale(glm::mat4 scale) {
    this->m_localScale = scale;
    updateLocalTransform();
}

void SceneObject::setBaseTransform(glm::mat4 transform) {
    this->m_baseTransform = transform;
}

void SceneObject::rotate(glm::vec3 eulerAngles) {
    this->m_localTransform = getRotationMatrix(eulerAngles) * m_localTransform;
}

void SceneObject::rotate(glm::mat4 rotation) {
    this->m_localTransform = rotation * m_localTransform;
}

void SceneObject::translate(glm::vec3 translation) {
    this->m_localTransform = getTranslationMatrix(translation) * m_localTransform;
}

void SceneObject::translate(glm::mat4 translation) {
    this->m_localTransform = translation * m_localTransform;
}

void SceneObject::setLocalRotation(glm::vec3 angles) {
    this->m_localRotation = getRotationMatrix(angles);
    updateLocalTransform();
}

void SceneObject::setLocalTranslation(glm::vec3 vec) {
    this->m_localTranslation = getTranslationMatrix(vec);
    updateLocalTransform();
}

void SceneObject::setLocalScale(glm::vec3 factors) {
    this->m_localScale = getScaleMatrix(factors);
    updateLocalTransform();
}


