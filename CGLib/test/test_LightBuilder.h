//
// Created by ubuntu on 24/12/20.
//

#ifndef CGPROJECT_TEST_LIGHTBUILDER_H
#define CGPROJECT_TEST_LIGHTBUILDER_H

void test_build_directional();
void test_build_omnidirectional();
void test_build_spotlight();

void suite_lightbuilder();

#endif //CGPROJECT_TEST_LIGHTBUILDER_H
