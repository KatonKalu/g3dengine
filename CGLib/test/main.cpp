//////////////
// #INCLUDE //
//////////////

//GLM:
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"
#include <glm/gtc/type_ptr.hpp>

// C/C++:
#include <iostream>
#include <SceneGraph.h>
#include <cglib.h>
#include <FileParser.h>
#include <OVOStrategy.h>
#include <CameraManager.h>
#include <TextureManager.h>
#include <AnisotropicFilteringStrategy.h>
#include <LinearFilteringStrategy.h>
#include <NoFilteringStrategy.h>
#include <BilinearFilteringStrategy.h>
#include <TrilinearFilteringStrategy.h>

#include "test_LightManager.h"
#include "test_LightBuilder.h"
#include "test_CameraManager.h"
#include "test_MaterialManager.h"


void callback_display() {}


void execute_test_suites() {
    suite_lightmanager();
    suite_lightbuilder();
    suite_cameramanager();
    suite_materialmanager();
}

/**
 * Application entry point.
 * @param argc number of command-line arguments passed 
 * @param argv array containing up to argc passed arguments
 * @return error code (0 on success, error code otherwise)
 */
int main(int argc, char *argv[]) {

    //Init context
    CGLib::setWindowOptions(CGLIB_RGBA | CGLIB_DOUBLE_BUFFER | CGLIB_DEPTH_BUFFER, 1000, 500);
    CGLib::init("CGLib", &argc, argv);

    CGLib::createWindow();
    CGLib::setDisplayCallback(callback_display);

    execute_test_suites();

    std::cout << "\n[application terminated - tests ran successfully]\n" << std::endl;
    return 0;
}

