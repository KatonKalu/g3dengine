//
// Created by purpleknight on 12/24/20.
//
#include <CameraManager.h>
#include "test_CameraManager.h"

void test_add_camera(Camera *toAdd, int expectedSize) {
    CameraManager &cameraManager = CameraManager::getInstance();

    cameraManager.add(toAdd);
    test_camera_list_size(expectedSize);
}

void test_remove_camera(Camera *toRemove, int expectedSize) {
    CameraManager &cameraManager = CameraManager::getInstance();

    cameraManager.remove(toRemove);
    test_camera_list_size(expectedSize);
}

void test_active_camera(Camera *expected) {
    CameraManager &cameraManager = CameraManager::getInstance();

    assert(cameraManager.getActiveCamera() == expected);
}

void test_active_camera_not_null() {
    CameraManager &cameraManager = CameraManager::getInstance();

    assert(cameraManager.getActiveCamera() != nullptr);
}

void test_camera_list_size(int expected) {
    CameraManager &cameraManager = CameraManager::getInstance();

    assert(cameraManager.size() == expected);
}

void test_camera_is_present(Camera *camera) {
    CameraManager &cameraManager = CameraManager::getInstance();
    assert(cameraManager.isPresent(camera));
}

void test_enable_disable(Camera *camera, Camera *defaultCamera) {
    CameraManager &cameraManager = CameraManager::getInstance();

    assert(cameraManager.enable(camera) == 0);

    test_add_camera(camera, 1);

    test_active_camera_not_null();

    assert(cameraManager.enable(camera) != 0);
    test_active_camera(camera);

    assert(cameraManager.disable() != 0);

    test_camera_list_size(1);

    assert(cameraManager.disable() == 0);
}


void suite_cameramanager() {
    test_camera_list_size(0);
    test_active_camera_not_null();

    Camera defaultCamera{};

    Camera c1{};
    c1.setCenter(glm::vec3{1.5f, 100.0f, 11.0f});
    c1.setUp(glm::vec3{1.5f, 50.0f, 2.0f});

    test_enable_disable(&c1, &defaultCamera);

    test_camera_is_present(&c1);

    test_remove_camera(&c1, 0);

}
