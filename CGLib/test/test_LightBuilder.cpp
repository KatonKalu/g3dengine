//
// Created by ubuntu on 24/12/20.
//

#include <glm/glm.hpp>
#include <LightBuilder.h>

void test_build_directional(){

    glm::vec3 translation{5.0f};
    glm::vec3 rotation{5.0f};
    glm::vec3 scaling{5.0f};

    glm::vec4 ambient{0.7f};
    glm::vec4 diffuse{0.7f};
    glm::vec4 specular{0.7f};

    float constant = 0.5f;
    float linear = 0.5f;
    float quadratic = 0.5f;

    DirectionalLight *l = LightBuilder::getInstance()
            .begin()
            .setTransform(translation, rotation, scaling)
            .setLightComponents(ambient, diffuse, specular)
            .setAttenuationComponents(constant, linear, quadratic)
            .buildDirectional();

    assert(l->getLightRef() == 0);
    assert(l->getTranslation() == getTranslationMatrix(translation));
    assert(l->getRotation() == getRotationMatrix(rotation));
    assert(l->getScaling() == getScaleMatrix(scaling));
    assert(l->getAmbient() == ambient);
    assert(l->getDiffuse() == diffuse);
    assert(l->getSpecular() == specular);
    assert(l->getConstantAttenuation() == constant);
    assert(l->getLinearAttenuation() == linear);
    assert(l->getQuadraticAttenuation() == quadratic);
    assert(l->getPosition().w == 0.0f);

}

void test_build_omnidirectional(){

    glm::vec3 translation{5.0f};
    glm::vec3 rotation{5.0f};
    glm::vec3 scaling{5.0f};

    glm::vec4 ambient{0.7f};
    glm::vec4 diffuse{0.7f};
    glm::vec4 specular{0.7f};

    float constant = 0.5f;
    float linear = 0.5f;
    float quadratic = 0.5f;

    float cutoff = 180.0f;
    glm::vec3 direction = glm::vec3{1.0f, 0.0f, 0.0f};

    OmniDirectionalLight *l = LightBuilder::getInstance()
            .begin()
            .setTransform(translation, rotation, scaling)
            .setLightComponents(ambient, diffuse, specular)
            .setAttenuationComponents(constant, linear, quadratic)
            .buildOmniDirectional();

    assert(l->getLightRef() == 0);
    assert(l->getTranslation() == getTranslationMatrix(translation));
    assert(l->getRotation() == getRotationMatrix(rotation));
    assert(l->getScaling() == getScaleMatrix(scaling));
    assert(l->getAmbient() == ambient);
    assert(l->getDiffuse() == diffuse);
    assert(l->getSpecular() == specular);
    assert(l->getConstantAttenuation() == constant);
    assert(l->getLinearAttenuation() == linear);
    assert(l->getQuadraticAttenuation() == quadratic);
    assert(l->getPosition().w == 1.0f);
    assert(l->getCutoff() == cutoff);

}

void test_build_spotlight(){

    glm::vec3 translation{5.0f};
    glm::vec3 rotation{5.0f};
    glm::vec3 scaling{5.0f};

    glm::vec4 ambient{0.7f};
    glm::vec4 diffuse{0.7f};
    glm::vec4 specular{0.7f};

    float constant = 0.5f;
    float linear = 0.5f;
    float quadratic = 0.5f;

    float cutoff = 60.0f;
    glm::vec3 direction = glm::vec3{1.0f, 0.0f, 0.0f};

    SpotLight *l = LightBuilder::getInstance()
            .begin()
            .setTransform(translation, rotation, scaling)
            .setLightComponents(ambient, diffuse, specular)
            .setAttenuationComponents(constant, linear, quadratic)
            .setCutoff(cutoff)
            .buildSpot();

    assert(l->getLightRef() == 0);
    assert(l->getTranslation() == getTranslationMatrix(translation));
    assert(l->getRotation() == getRotationMatrix(rotation));
    assert(l->getScaling() == getScaleMatrix(scaling));
    assert(l->getAmbient() == ambient);
    assert(l->getDiffuse() == diffuse);
    assert(l->getSpecular() == specular);
    assert(l->getConstantAttenuation() == constant);
    assert(l->getLinearAttenuation() == linear);
    assert(l->getQuadraticAttenuation() == quadratic);
    assert(l->getPosition().w == 1.0f);
    assert(l->getCutoff() == cutoff);

}

void suite_lightbuilder(){

    test_build_directional();
    test_build_omnidirectional();
    test_build_spotlight();

}