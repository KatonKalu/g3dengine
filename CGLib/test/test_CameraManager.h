//
// Created by purpleknight on 12/24/20.
//

#ifndef CGPROJECT_TEST_CAMERAMANAGER_H
#define CGPROJECT_TEST_CAMERAMANAGER_H

#include <Camera.h>

void test_add_camera(Camera *toAdd, int expectedSizeAfterAdd);
void test_remove_camera(Camera *toRemove, int expectedSizeAfterRemove);
void test_camera_is_present(Camera *camera);
void test_enable_disable(Camera *camera, Camera *defaultCamera);
void test_camera_list_size(int expected);
void test_active_camera(Camera *expected);
void test_active_camera_not_null();

void suite_cameramanager();

#endif //CGPROJECT_TEST_CAMERAMANAGER_H
