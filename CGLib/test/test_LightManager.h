//
// Created by ubuntu on 23/12/20.
//

#ifndef CGPROJECT_TEST_LIGHTMANAGER_H
#define CGPROJECT_TEST_LIGHTMANAGER_H

void test_add_remove_size();
void test_ispresent();
void test_getlight();
void test_enable_disable();
void test_getactivelight();

void suite_lightmanager();

#endif //CGPROJECT_TEST_LIGHTMANAGER_H
