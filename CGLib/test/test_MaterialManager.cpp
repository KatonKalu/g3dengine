//
// Created by kalu on 12/24/20.
//

#include <MaterialManager.h>
#include "test_MaterialManager.h"
#include "Material.h"

void suite_materialmanager() {
    auto* m = new Material();
    MaterialManager::getInstance().addMaterial("testMat", m);
    auto *found = MaterialManager::getInstance().getMaterial("testMat");

    assert(m == found);

    delete m;
}