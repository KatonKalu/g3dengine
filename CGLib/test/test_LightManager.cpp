//
// Created by ubuntu on 23/12/20.
//

#include <LightBuilder.h>
#include <LightManager.h>
#include "test_LightManager.h"

void test_add_remove_size(){

    LightManager instance = LightManager::getInstance();

    Light *l1 = LightBuilder::getInstance()
            .buildDirectional();

    assert(instance.size() == 0);

    instance.add(l1);

    assert(instance.size() == 1);

    instance.remove(l1);

    assert(instance.size() == 0);


}

void test_ispresent(){

    LightManager instance = LightManager::getInstance();

    Light *l1 = LightBuilder::getInstance()
            .buildDirectional();

    assert(instance.isPresent(l1) == false);

    instance.add(l1);

    assert(instance.isPresent(l1) == true);

    instance.remove(l1);

    assert(instance.isPresent(l1) == false);

}

void test_getlight(){

    LightManager instance = LightManager::getInstance();

    Light *l1 = LightBuilder::getInstance()
            .buildDirectional();

    instance.add(l1);

    assert(instance.getLight(l1->getId()) == l1);

    instance.remove(l1);

    assert(instance.getLight(l1->getId()) == nullptr);

}

void test_enable_disable(){

    LightManager instance = LightManager::getInstance();

    Light *l1 = LightBuilder::getInstance()
            .buildDirectional();

    instance.add(l1);
    instance.enable(l1);

    assert(l1->getLightRef() != 0);

    instance.disable(l1);

    assert(l1->getLightRef() == 0);

    instance.remove(l1);

}

void test_getactivelight(){

    LightManager instance = LightManager::getInstance();

    Light *l1 = LightBuilder::getInstance()
            .buildDirectional();

    instance.add(l1);
    instance.enable(l1);

    assert(instance.getActiveLight(l1->getId()) == l1);

    instance.disable(l1);

    assert(instance.getActiveLight(l1->getId()) == nullptr);

    instance.remove(l1);

}

void suite_lightmanager(){

    test_add_remove_size();
    test_ispresent();
    test_getlight();
    test_enable_disable();
    test_getactivelight();

}