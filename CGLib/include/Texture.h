//
// Created by ubuntu on 21/12/20.
//

#ifndef CGPROJECT_TEXTURE_H
#define CGPROJECT_TEXTURE_H

#pragma region DLL_DEFS

#include <vector>
#include <glm/glm.hpp>

#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

class LIB_API Texture {

private:

    unsigned int m_tex_id;

    unsigned char *m_bitmap;

    unsigned int m_width;

    unsigned int m_height;

public:
    virtual ~Texture();

public:

    void loadToGPU();

    [[nodiscard]] const unsigned int getTexId() const;

    [[nodiscard]] unsigned char * getBitmap() const;

    [[nodiscard]] unsigned int getHeight() const;

    [[nodiscard]] unsigned int getWidth() const;

    void setBitmap(unsigned char *mBitmap);

    void setWidth(unsigned int mWidth);

    void setHeight(unsigned int mHeight);

    void clean();

};


#endif //CGPROJECT_TEXTURE_H
