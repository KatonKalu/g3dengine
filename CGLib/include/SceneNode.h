//
// Created by kalu on 22/10/20.
//

#ifndef CGPROJECT_CGLIB_INCLUDE_SCENENODEOBJECT_H_
#define CGPROJECT_CGLIB_INCLUDE_SCENENODEOBJECT_H_

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "SceneObject.h"

class LIB_API SceneNode : public SceneObject {
private:

    SceneNode *m_parent;
    std::vector<SceneNode *> m_children;

//    void renderChildren(glm::mat4 cameraMatrix);

public:

    /**
   * Constructor for a scene node (every type of object in the scene)
   * @param mesh is the mesh ( no transforms ) used to represent model
   * @param rotate the vector of the euler angles ( default: 0,0,0 )
   * @param translate m_localTranslation vector ( default: 0,0,0 )
   * @param scale m_localScale vector ( default: 1,1,1 )
   */

    SceneNode(const glm::vec3 &rotate,
              const glm::vec3 &translate,
              const glm::vec3 &scale);

/**
 * Add a child to this node in the scene hierarchy
 * @param n the node to add
 */
    void addChild(SceneNode *n);

    /**
     * Get the select child at index m_position
     * @param index the index in the m_children list
     * @return found child
     */
    SceneNode *getChild(int index);

    /**
     * @return the list of all m_children
     */
    std::vector<SceneNode *> getChildren();

    [[nodiscard]] SceneNode *getParent() const;

    /**
     * Render m_vertices with transforms within the scene for the current frame
     * @param cameraMatrix is the view matrix used to display objects
     */
    virtual void render(glm::mat4 cameraMatrix) = 0;

    [[nodiscard]] glm::mat4 getTotalTransform() const;

    [[nodiscard]] SceneNode * findNodeInChildren(std::string nodeId) const;

    void removeChildren(const std::string& nodeId);
};

#endif
