//
// Created by kalu on 12/3/20.
//

#ifndef CGLIB_CGLIB_INCLUDE_EMPTYNODE_H_
#define CGLIB_CGLIB_INCLUDE_EMPTYNODE_H_

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions
#include "SceneNode.h"

class LIB_API EmptyNode: public SceneNode {
 public:
  explicit EmptyNode(const glm::vec3 &rotate = glm::vec3(0.0f),
      const glm::vec3 &translate = glm::vec3(0.0f),
      const glm::vec3 &scale = glm::vec3(1.0f));

  void render(glm::mat4 cameraMatrix) override;
};

#endif //CGLIB_CGLIB_INCLUDE_EMPTYNODE_H_
