//
// Created by purpleknight on 11/16/20.
//

#ifndef CGPROJECT_MESH_H
#define CGPROJECT_MESH_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include <utility>

#include "Material.h"
#include "SceneNode.h"
#include "Texture.h"

//TODO add destructor

class LIB_API Mesh : public SceneNode {

private:

    Material *m_material;
    int m_verticesNumber;
    int m_trianglesNumber;
    const glm::vec3 *m_vertices;
    const unsigned int *m_triangles;
    const glm::vec2 *m_mapping;
    const glm::vec3 *m_normals;

    void renderMesh() const;

public:

    Mesh(const glm::vec3 *vertices,
         const unsigned int *triangles,
         int vertices_number,
         int triangles_number,
         Material *material = new Material{},
         const glm::vec3 &rotate = glm::vec3{0.0f},
         const glm::vec3 &translate = glm::vec3{0.0f},
         const glm::vec3 &scale = glm::vec3{1.0f})
            : SceneNode(rotate, translate, scale),
              m_vertices(vertices),
              m_triangles(triangles),
              m_material(material),
              m_trianglesNumber(triangles_number),
              m_verticesNumber(vertices_number) {
        setId(getId().append("mesh"));
    }

    ~Mesh();

    void setNormals(glm::vec3* normals);

    [[nodiscard]] const unsigned int *getTriangles() const;

    [[nodiscard]] const glm::vec3 *getVertices() const;

    [[nodiscard]] const glm::vec3 *getVertexInTriangle(int triangleIndex, int vertexIndex) const;

    [[nodiscard]] const Material *getMaterial() const;

    void setMaterial(Material *material);

    void render(glm::mat4 camera) override;

    [[nodiscard]] const glm::vec2 *getMapping() const;

    void setMapping(const glm::vec2 *mMapping);

    [[nodiscard]] int getVerticesNumber() const;

    [[nodiscard]] int getFacesNumber() const;

};

#endif
