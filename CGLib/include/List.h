//
// Created by purpleknight on 12/6/20.
//

#ifndef CGPROJECT_LIST_H
#define CGPROJECT_LIST_H

#pragma region DLL_DEFS

#include <list>
#include "SceneNode.h"

#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions


class LIB_API List {
private:
    std::list<SceneNode *> elements;
public:
    List() : elements() {}

    void pass(SceneNode* node);
    void add(SceneNode *node);
    void render(glm::mat4 cameraMatrix);
};


#endif //CGPROJECT_LIST_H
