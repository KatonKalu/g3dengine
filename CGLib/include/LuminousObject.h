//
// Created by ubuntu on 23/11/20.
//

#ifndef CGPROJECT_LUMINOUSOBJECT_H
#define CGPROJECT_LUMINOUSOBJECT_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include <glm/glm.hpp>

class LIB_API LuminousObject {
private:
    glm::vec4 m_diffuse, m_ambient, m_specular, m_emission;

public:

    LuminousObject(
            const glm::vec4 &diffuse,
            const glm::vec4 &ambient,
            const glm::vec4 &specular,
            const glm::vec4 &emission = glm::vec4{0.0f,0.0f,0.0f,1.0f});

    [[nodiscard]] const glm::vec4 &getDiffuse() const;

    void setDiffuse(const glm::vec4 &diffuse);
    void setDiffuse(const glm::vec3 &diffuse);

    [[nodiscard]] const glm::vec4 &getAmbient() const;

    void setAmbient(const glm::vec4 &ambient);
    void setAmbient(const glm::vec3 &ambient);

    [[nodiscard]] const glm::vec4 &getSpecular() const;

    void setSpecular(const glm::vec4 &specular);
    void setSpecular(const glm::vec3 &specular);

    virtual void apply() = 0;

    const glm::vec4 &getEmission() const;

    void setEmission(const glm::vec4 &emission);

    void setEmission(const glm::vec3 &emission);
};


#endif
