//
// Created by kalu on 12/25/20.
//

#ifndef CGLIB_TRILINEARFILTERINGSTRATEGY_H
#define CGLIB_TRILINEARFILTERINGSTRATEGY_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "LinearFilteringStrategy.h"

class LIB_API TrilinearFilteringStrategy : public LinearFilteringStrategy {
    void execute() override;
};


#endif //CGLIB_TRILINEARFILTERINGSTRATEGY_H
