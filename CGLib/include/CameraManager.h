//
// Created by purpleknight on 12/22/20.
//

#include "Camera.h"

#ifndef CGPROJECT_CAMERAMANAGER_H
#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#define CGPROJECT_CAMERAMANAGER_H


class LIB_API CameraManager {
private:
    Camera *m_default_camera;
    Camera *m_active_camera;
    std::vector<Camera *> m_cameras;

    CameraManager();

public:
    static CameraManager &getInstance() {

        static CameraManager instance{};

        return instance;
    };

    [[nodiscard]] const std::vector<Camera *> &getCameras() const;

    void add(Camera* camera);

    void remove(Camera* toRemove);

    Camera* getActiveCamera();

    int enable(Camera* camera);

    int disable();

    bool isPresent(Camera* camera);

    int size();

};


#endif //CGPROJECT_CAMERAMANAGER_H
