#include "ParsingStrategy.h"

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else
#define LIB_API  
#endif
#pragma endregion DLL Macros Definitions

#include "SceneNode.h"

class LIB_API OVOStrategy : public ParsingStrategy {
private:
    virtual SceneNode* parse(FILE* fp);
public:
    OVOStrategy() = default;;
    void execute() override;
};