//
// Created by kalu on 12/25/20.
//

#ifndef CGLIB_ISTRATEGY_H
#define CGLIB_ISTRATEGY_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions


class LIB_API IStrategy{
public:
    virtual void execute() = 0;
    virtual ~IStrategy(){}
};

#endif //CGLIB_ISTRATEGY_H