//
// Created by kalu on 12/25/20.
//

#ifndef CGLIB_ITEMPLATE_H
#define CGLIB_ITEMPLATE_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

class LIB_API ITemplate {
public:
    virtual void execute() = 0;
};


#endif //CGLIB_ITEMPLATE_H
