//
// Created by purpleknight on 11/16/20.
//

#ifndef CGPROJECT_LIGHT_H
#define CGPROJECT_LIGHT_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "SceneNode.h"
#include "LuminousObject.h"

class LIB_API Light : public SceneNode, public LuminousObject {

private:

    unsigned int m_light_ref;
    float m_linear, m_constant, m_quadratic;

    friend void setLightRef(Light *light, unsigned int lightRef);

protected:
    float m_position;
public:

    explicit Light(unsigned int light_ref = 0,
                   const glm::vec3 &rotate = glm::vec3{0.0f},
                   const glm::vec3 &translate = glm::vec3{0.0f},
                   const glm::vec3 &scale = glm::vec3{1.0f},
                   const glm::vec4 &diffuse = glm::vec4{0.5f},
                   const glm::vec4 &ambient = glm::vec4{0.0f},
                   const glm::vec4 &specular = glm::vec4{1.0f},
                   float position = 0.0f,
                   float linear = 0,
                   float constant = 1,
                   float quadratic = 0);

    [[nodiscard]] virtual glm::vec4 getPosition() const;

    void setPosition(float position);

    [[nodiscard]] float getLinearAttenuation() const;

    void setLinearAttenuation(float linear);

    [[nodiscard]] float getConstantAttenuation() const;

    void setConstantAttenuation(float constant);

    [[nodiscard]] float getQuadraticAttenuation() const;

    void setQuadraticAttenuation(float quadratic);

    [[nodiscard]] unsigned int getLightRef() const;

    void setInfluence(float radius);

    void enable() const;

    void disable() const;

    void apply() override;

    void render(glm::mat4 cameraMatrix) override;

};


#endif
