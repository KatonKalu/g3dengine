//
// Created by ubuntu on 29/11/20.
//

#ifndef CGPROJECT_SPOTLIGHT_H
#define CGPROJECT_SPOTLIGHT_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "Light.h"

class LIB_API SpotLight : public Light {

 private:
  float m_cutoff;
  glm::vec3 m_direction;

 public:

  explicit SpotLight(unsigned int lightRef = 0,
                     const glm::vec3 &rotate = glm::vec3{0.0f},
                     const glm::vec3 &translate = glm::vec3{0.0f},
                     const glm::vec3 &scale = glm::vec3{1.0f},
                     const glm::vec4 &diffuse = glm::vec4{1.0f},
                     const glm::vec4 &ambient = glm::vec4{1.0f},
                     const glm::vec4 &specular = glm::vec4{1.0f},
                     float linear = 0,
                     float constant = 1,
                     float quadratic = 0,
                     float cutoff = 45.0f,
                     glm::vec3 direction = glm::vec3{0.0f});

  [[nodiscard]] glm::vec3 getDirection() const;
  [[nodiscard]] float getCutoff() const;

  void setDirection(const glm::vec3 &m_direction);
  void setCutoff(float m_cutoff);

  void apply() override;

};

#endif //CGPROJECT_SPOTLIGHT_H
