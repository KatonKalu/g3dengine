//
// Created by ubuntu on 30/11/20.
//

#ifndef CGPROJECT_LIGHTBUILDER_H
#define CGPROJECT_LIGHTBUILDER_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "DirectionalLight.h"
#include "OmniDirectionalLight.h"

class LIB_API LightBuilder {

private:

    glm::vec3 m_translation{0.0f};
    glm::vec3 m_rotation{0.0f};
    glm::vec3 m_scaling{1.0f};

    glm::vec4 m_ambient{1.0f};
    glm::vec4 m_diffuse{1.0f};
    glm::vec4 m_specular{1.0f};

    float m_constant = 1.0f;
    float m_linear = 0.0f;
    float m_quadratic = 0.0f;

    float m_cutoff{};
    glm::vec3 m_direction{};

    LightBuilder() = default;

public:

    static LightBuilder &getInstance() {

        static LightBuilder instance{};

        return instance;

    }

    LightBuilder &begin();

    LightBuilder &setTransform(glm::vec3 translate, glm::vec3 rotate, glm::vec3 scale);

    LightBuilder &setTranslation(glm::vec3 translate);

    LightBuilder &setRotation(glm::vec3 rotate);

    LightBuilder &setScaling(glm::vec3 scale);

    LightBuilder &setLightComponents(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular);

    LightBuilder &setAmbient(glm::vec4 ambient);

    LightBuilder &setDiffuse(glm::vec4 diffuse);

    LightBuilder &setSpecular(glm::vec4 specular);

    LightBuilder &setAttenuationComponents(float constant, float linear, float quadratic);

    LightBuilder &setConstantAttenuation(float constant);

    LightBuilder &setLinearAttenuation(float linear);

    LightBuilder &setQuadraticAttenuation(float quadratic);

    LightBuilder &setCutoff(float cutoff);

    LightBuilder &setDirection(glm::vec3 direction);

    DirectionalLight *buildDirectional();

    OmniDirectionalLight *buildOmniDirectional();

    SpotLight *buildSpot();

};

#endif //CGPROJECT_LIGHTBUILDER_H
