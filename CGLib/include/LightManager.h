//
// Created by ubuntu on 30/11/20.
//

#ifndef CGPROJECT_LIGHTMANAGER_H
#define CGPROJECT_LIGHTMANAGER_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include <map>
#include "Light.h"

class LIB_API LightManager {

private:

    int m_supported_lights;

    glm::vec4 m_global_ambient = glm::vec4{0.2f, 0.2f, 0.2f, 1.0f};

    std::map<unsigned int, Light *> m_active_lights;

    std::vector<Light *> m_lights;

    friend std::map<unsigned int, Light *> initializeActiveLightsMap();

    friend unsigned int getFirstAvailableSpot();

    LightManager();

public:

    static LightManager &getInstance() {

        static LightManager instance{};

        return instance;

    };

    /**
     * Returns the number of lights supported by OpenGL, <= 8
     * @return
     */
    [[nodiscard]] int getSupportedLights() const;

    /**
     * Returns a pointer to a a light (not necessarily active) stored in m_lights,
     * by comparing the given string with the lights' ids.
     * @param id
     * @return pointer to light on success, nullptr on failure
     */
    [[nodiscard]] Light *getLight(const std::string &id) const;
    
    /**
    * Check wheater the given light is present or not in the lightManager
    */
    bool isPresent(Light* light) const;

    /**
     * Returns a pointer to a currently active light stored in m_active_lights,
     * by comparing the given string with the lights' ids.
     * @param id
     * @return pointer to light on success, nullptr on failure
     */
    [[nodiscard]] Light *getActiveLight(const std::string &id) const;

    /**
     * Returns a pointer to a currently active light stored in m_active_lights,
     * by accessing the map with the specified key. Throws out_of_range exception if
     * the index does not exist within the map.
     * Indexes are always in the range [ CGLIB_LIGHT0 , CGLIB_LIGHT0 + getSupportedLights() [
     * @param lightRef
     * @return pointer to light
     */
    [[nodiscard]] Light *getActiveLight(unsigned int lightRef) const;

    /**
     * Enables a light for rendering by adding it to the active lights map.
     * Fails if it is not recognized or if it is already enabled.
     * @param light
     * @return key compatible as an index of the map containing active lights,
     * 0 in case the light is already enabled or is not stored in the lights' container
     */
    unsigned int enable(Light *light);

    /**
     * Enables a light for rendering by adding it to the active lights map.
     * Fails if it is not recognized or if it is already enabled.
     * @param id
     * @return key compatible as an index of the map containing active lights,
     * 0 in case the light is already enabled or is not stored in the lights' container
     */
    unsigned int enable(const std::string &id);

    /**
     * Disables a light from rendering by removing it from the active lights map and setting its light reference to 0.
     * Fails if it is not recognized.
     * @param light
     * @return index used by the now disabled light, for future use.
     * 0 in case the light is already disabled.
     */
    unsigned int disable(Light *light);

    /**
     * Disables a light from rendering by removing it from the active lights map and setting its light reference to 0.
     * Fails if it is not recognized.
     * @param id
     * @return index used by the now disabled light, for future use.
     * 0 in case the light is already disabled.
     */
    unsigned int disable(const std::string &id);

    /**
     * Pushes a light to the back of the vector responsible for storing lights
     * @param light
     */
    void add(Light *light);

    /**
     * Removes a light from the vector responsible for storing lights
     * @param id
     */
    void remove(const std::string &id);

    /**
     * Removes a light from the vector responsible for storing lights
     * @param light
     */
    void remove(Light *light);

    /**
     * Returns the number of active lights
     * @return
     */
    int enabled();

    std::vector<Light*> getAllLights();

    /**
     * Returns how many lights the manager has accepted in total
     * @return
     */
    int size();

    void setGlobalAmbient(const glm::vec4 &globalAmbient);
};


#endif //CGPROJECT_LIGHTMANAGER_H
