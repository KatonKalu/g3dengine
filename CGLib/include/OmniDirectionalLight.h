//
// Created by ubuntu on 29/11/20.
//

#ifndef CGPROJECT_OMNIDIRECTIONALLIGHT_H
#define CGPROJECT_OMNIDIRECTIONALLIGHT_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "SpotLight.h"

class LIB_API OmniDirectionalLight : public Light{

private:
    float m_cutoff;

public:

    explicit OmniDirectionalLight(unsigned int lightRef = 0,
                                  const glm::vec3 &rotate = glm::vec3{0.0f},
                                  const glm::vec3 &translate = glm::vec3{0.0f},
                                  const glm::vec3 &scale = glm::vec3{1.0f},
                                  const glm::vec4 &diffuse = glm::vec4{1.0f},
                                  const glm::vec4 &ambient = glm::vec4{1.0f},
                                  const glm::vec4 &specular = glm::vec4{1.0f},
                                  float linear = 0,
                                  float constant = 1,
                                  float quadratic = 0);

    void apply() override;

    [[nodiscard]] float getCutoff() const;

};


#endif //CGPROJECT_OMNIDIRECTIONALLIGHT_H
