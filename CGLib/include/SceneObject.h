//
// Created by purpleknight on 11/16/20.
//

#ifndef CGPROJECT_SCENEOBJECT_H
#define CGPROJECT_SCENEOBJECT_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include <random>
#include <sstream>
#include <glm/glm.hpp>
#include "transform.h"
#include <string>

class LIB_API SceneObject {

protected:
    std::string m_id;

    glm::mat4 m_baseTransform = glm::mat4{1.0f};

private:
    glm::mat4 m_localTransform{};
    glm::mat4 m_localRotation;
    glm::mat4 m_localScale;
    glm::mat4 m_localTranslation;

    static std::string generate_uuid_v4();

public:

    SceneObject(
            glm::vec3 rotate,
            glm::vec3 translate,
            glm::vec3 scale);

    [[nodiscard]] std::string getId() const;

    [[nodiscard]] glm::mat4 getRotation() const;

    [[nodiscard]] glm::mat4 getTranslation() const;

    [[nodiscard]] glm::mat4 getScaling() const;

    [[nodiscard]] glm::mat4 getLocalTransform() const;

    void updateLocalTransform();

    void setId(const std::string &mId);

    void setLocalRotation(glm::mat4 rotation);

    void setLocalTranslation(glm::mat4 translation);

    void setLocalScale(glm::mat4 scale);

    void setBaseTransform(glm::mat4 transform);

    virtual void rotate(glm::vec3 eulerAngles);

    void rotate(glm::mat4 rotation);

    virtual void translate(glm::vec3 translation);

    void translate(glm::mat4 translation);

    void setLocalRotation(glm::vec3 angles);

    void setLocalTranslation(glm::vec3 vec);

    void setLocalScale(glm::vec3 factors);
};

#endif
