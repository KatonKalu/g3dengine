//
// Created by kalu on 12/25/20.
//

#ifndef CGLIB_FILTERINGTEMPLATE_H
#define CGLIB_FILTERINGTEMPLATE_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "ITemplate.h"
#include "IStrategy.h"

class LIB_API FilteringTemplate: ITemplate {
private:
    IStrategy* m_pixelStrategy = nullptr;
    IStrategy* m_anisotropicStrategy = nullptr;
public:
    void execute() override;
    void setPerPixelStrategy(IStrategy* strategy);
    void setAnisotropicStrategy(IStrategy* strategy);
    IStrategy* getPerPixelStrategy() const;
    IStrategy* getAnisotropicStrategy() const;
    void disableAnisotropicStrategy();
    ~FilteringTemplate();
};


#endif //CGLIB_FILTERINGTEMPLATE_H
