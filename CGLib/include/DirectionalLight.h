//
// Created by ubuntu on 23/11/20.
//

#ifndef CGPROJECT_DIRECTIONALLIGHT_H
#define CGPROJECT_DIRECTIONALLIGHT_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "Light.h"

class LIB_API DirectionalLight : public Light {

public:

    explicit DirectionalLight(unsigned int light_ref = 0,
                              const glm::vec3 &rotate = glm::vec3{0.0f},
                              const glm::vec3 &translate = glm::vec3{0.0f},
                              const glm::vec3 &scale = glm::vec3{1.0f},
                              const glm::vec4 &diffuse = glm::vec4{1.0f},
                              const glm::vec4 &ambient = glm::vec4{1.0f},
                              const glm::vec4 &specular = glm::vec4{1.0f},
                              float linear = 0,
                              float constant = 1,
                              float quadratic = 0);

    void apply() override;

    [[nodiscard]] glm::vec4 getPosition() const override;

};


#endif
