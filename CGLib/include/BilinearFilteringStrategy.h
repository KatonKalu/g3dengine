//
// Created by kalu on 12/25/20.
//

#ifndef CGLIB_BILINEARFILTERINGSTRATEGY_H
#define CGLIB_BILINEARFILTERINGSTRATEGY_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "LinearFilteringStrategy.h"

class LIB_API BilinearFilteringStrategy: public LinearFilteringStrategy{
public:
    void execute() override;
};


#endif //CGLIB_BILINEARFILTERINGSTRATEGY_H
