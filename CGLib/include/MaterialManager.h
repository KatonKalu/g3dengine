//
// Created by kalu on 12/17/20.
//

#ifndef CGLIB_MATERIALMANAGER_H
#define CGLIB_MATERIALMANAGER_H

#include <string>
#include <unordered_map>
#include "Material.h"

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
   // Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions


class LIB_API MaterialManager {
private:
    std::unordered_map<std::string, Material*> materialMap{};
    MaterialManager() = default;
public:
    static MaterialManager& getInstance();
    void addMaterial(std::string name, Material *material);
    void removeMaterial(std::string name);
    Material* getMaterial(std::string name);
};

#endif //CGLIB_MATERIALMANAGER_H
