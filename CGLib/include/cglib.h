//
// Created by ubuntu on 22/10/20.
//

#ifndef CGPROJECT_CGLIB_H
#define CGPROJECT_CGLIB_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
   // Export API:
   // Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else
#define LIB_API  
#endif
#pragma endregion DLL Macros Definitions

/**
* Wrapped FreeGLUT DisplayMode options
*/
extern LIB_API unsigned int CGLIB_RGB;
extern LIB_API unsigned int CGLIB_RGBA;
extern LIB_API unsigned int CGLIB_SINGLE_BUFFER;
extern LIB_API unsigned int CGLIB_DOUBLE_BUFFER;
extern LIB_API unsigned int CGLIB_DEPTH_BUFFER;

/**
* Wrapped OpenGL options
*/
extern LIB_API const unsigned int  CGLIB_CLEAR_COLOR;
extern LIB_API const unsigned int  CGLIB_CLEAR_DEPTH;
extern LIB_API const unsigned int  CGLIB_DEPTH;
extern LIB_API const unsigned int  CGLIB_LIGHTING;
extern LIB_API const unsigned int  CGLIB_MMODE_MODELVIEW;
extern LIB_API const unsigned int  CGLIB_MMODE_PROJECTION;
extern LIB_API const unsigned int  CGLIB_CULLING;
extern LIB_API const unsigned int  CGLIB_WIREFRAME;
extern LIB_API const unsigned int  CGLIB_SOLID;
extern LIB_API const unsigned int  CGLIB_BOTH;
extern LIB_API const unsigned int  CGLIB_FRONT;
extern LIB_API const unsigned int  CGLIB_BACK;
extern LIB_API const unsigned int  CGLIB_POSITION;
extern LIB_API const unsigned int  CGLIB_SPOT_CUTOFF;
extern LIB_API const unsigned int  CGLIB_SPOT_DIRECTION;
extern LIB_API const unsigned int  CGLIB_ATTENUATION_CONSTANT;
extern LIB_API const unsigned int  CGLIB_ATTENUATION_LINEAR;
extern LIB_API const unsigned int  CGLIB_ATTENUATION_QUADRATIC;
extern LIB_API const unsigned int  CGLIB_AMBIENT;
extern LIB_API const unsigned int  CGLIB_DIFFUSE;
extern LIB_API const unsigned int  CGLIB_SPECULAR;
extern LIB_API const unsigned int  CGLIB_EMISSION;
extern LIB_API const unsigned int  CGLIB_SHININESS;
extern LIB_API const unsigned int  CGLIB_LIGHT0;

    namespace CGLib {
          //TODO add emissive component to materials

          /**
           * Initializes OpenGL context.
           *
           * @param contextName
           */
        LIB_API void init(const char* contextName, int* pargc = nullptr, char** argv = nullptr);

        /**
         * Sets parameters for main display window.
         *
         * @param displayMode several options such as color mode, depth buffer usage, and single or double buffer
         * @param windowW width of the main window
         * @param windowH height of the main window
         * @param windowX m_position of the window on the x axis (FreeGLUT coordinates)
         * @param windowY m_position of the window on the y axis (FreeGLUT coordinates)
         */
        LIB_API void setWindowOptions(unsigned int displayMode, int windowW, int windowH, int windowX = 0, int windowY = 0);

        /**
         * Wraps FreeGLUT function for creating a new
         * window and defines glut window settings
         * with the previously specified options.
         */
        LIB_API void  createWindow();

        /**
         * Wraps OpenGL function to set
         * background color as a base for glClear()
         * @param r
         * @param g
         * @param b
         * @param a
         */
        LIB_API void  setBackgroundColor(float r, float g, float b, float a = 1.0f);

        /**
         * Wraps OpenGL function to clear screen with
         * previously specified background color.
         * Detects if z buffer is used and clears it accordingly.
         */
        LIB_API void clear();

        /**
         * Wraps FreeGLUT function to set
         * callback for window redisplay.
         *
         * @param callback
         */
        LIB_API void setDisplayCallback(void (*callback)(void));

        /**
         * Wrap FreeGLUT function to set
         * callback for window redisplay.
         *
         * @param callback
         */
        LIB_API void  setWindowReshapeCallback(void (*callback)(int width, int height));

        LIB_API void  setMouseCallback(void (*callback)(int mouseX, int mouseY));

        /**
         * Wraps FreeGLUT function to set
         * callback for keyboard events.
         * Only works on keys that return ASCII character codes.
         *
         * @param callback
         */
        LIB_API void  setKeyboardEventCallback(void (*callback)(unsigned char, int x, int y));

        /**TODO move to light manager
         * Specifies the glEnable / glDisable parameter as CGLIB_LIGHTING
         */
        LIB_API void  enableLights();

        LIB_API void  disableLights();

        /**
         * Specifies the glEnable / glDisable parameter as CGLIB_DEPTH
         */
        LIB_API void  enableDepthTest();

        LIB_API void  disableDepthTest();

        /**
         * Specifies the glEnable / glDisable parameter as CGLIB_CULLING
         */
        LIB_API void  enableFaceCulling();

        LIB_API void  disableFaceCulling();


        /**
         * Choose whether to render the FPS Counter or not.
         * The FPS are updated regardless.
         */
        LIB_API void enableFpsCounter();

        LIB_API void disableFpsCounter();

        /**
         * Allows switching between solid and wireframe rendering for the specified face
         * @param face
         * @param mode
         */
        LIB_API void  setPolygonMode(unsigned int face, unsigned int mode);

        /**
         * (Mostly) Used to update the projection matrix on a window reshape
         * @param width
         * @param height
         */
        LIB_API void  updateProjectionMatrix(int width, int height);

        /**
         * Used to force a refresh of the currently displayed scene.
         */
        LIB_API void  refreshDisplay();

        /**
         * Wraps FreeGLUT function to enter main application
         * loop. Also sets the option to close the window and to return
         * after the loop ends.
         */
        LIB_API void enterMainLoop();

        LIB_API void enableSmoothShading();

        LIB_API void enableFlatShading();

        LIB_API void setAssetsDirPath(const char *dirPath);

        LIB_API const char* getAssetsDirPath();


    }

inline char separator()
{
#ifdef _WIN32
    return '\\';
#else
    return '/';
#endif
}

#endif
