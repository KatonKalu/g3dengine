//
// Created by kalu on 12/10/20.
//

#ifndef CGPROJECT_TRANSFORM_H
#define CGPROJECT_TRANSFORM_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
   // Export API:
   // Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else
#define LIB_API  
#endif
#pragma endregion DLL Macros Definitions

#include <glm/glm.hpp>

/**
 * Get isotropic scaling matrix
 * @param scale the m_localScale factor
 * @return the 4x4 transform matrix
 */
LIB_API glm::mat4  getScaleMatrix(float scale);

/**
 * Get scaling matrix
 * @param scale the m_localScale factor for x,y,z
 * @return the 4x4 transform matrix
 */
LIB_API glm::mat4  getScaleMatrix(glm::vec3 scale);

/**
 * Get m_localTranslation matrix
 * @param Xunits m_localTranslation on x
 * @param Yunits m_localTranslation on y
 * @param Zunits m_localTranslation on z
 * @return the 4x4 transform matrix
 */
LIB_API glm::mat4  getTranslationMatrix(float Xunits, float Yunits, float Zunits);
LIB_API glm::mat4  getTranslationMatrix(glm::vec3 translate);

/**
 * Get m_localRotation matrix on X axis
 * @param degrees the angle to rotate
 * @return the 4x4 transform matrix
 */
LIB_API glm::mat4  getRotationMatrixX(float degrees);

/**
 * Get m_localRotation matrix on Y axis
 * @param degrees the angle to rotate
 * @return the 4x4 transform matrix
 */
LIB_API glm::mat4  getRotationMatrixY(float degrees);

/**
 * Get m_localRotation matrix on Z axis
 * @param degrees the angle to rotate
 * @return the 4x4 transform matrix
 */
LIB_API glm::mat4  getRotationMatrixZ(float degrees);

/**
 * Get m_localRotation matrix given eulerAngles
 * @param eulerAngles
 * @return transform matrix
 */
LIB_API glm::mat4  getRotationMatrix(glm::vec3 eulerAngles);

/**
 * Get camera matrix
 * @param eye m_position of the camera
 * @param center m_position of the point at which the camera is looking
 * @param up vector indicating the orientation of the world
 * @return 4v4 camera matrix
 */
LIB_API glm::mat4  getCameraMatrix(glm::vec3 eye, glm::vec3 center, glm::vec3 up);

/**
 * Get perspective matrix
 * @param fov field of view
 * @param aratio aspect ratio
 * @param nearplane nearplane distance
 * @param farplane farplane distance
 * @return 4v4 projection matrix
 */
LIB_API glm::mat4  getProjectionMatrix(float fov, float aratio, float nearplane, float farplane);

/**
 * Inverse of a matrix
 * @param matrix to invert
 * @return inverted matrix
 */
LIB_API glm::mat4  getInverseMatrix(glm::mat4 matrix);

#endif
