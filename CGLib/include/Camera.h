//
// Created by purpleknight on 11/16/20.
//

#ifndef CGPROJECT_CAMERA_H
#define CGPROJECT_CAMERA_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
   // Export API:
   // Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else
#define LIB_API  
#endif
#pragma endregion DLL Macros Definitions

#include "SceneNode.h"

class LIB_API Camera : public SceneNode {
private:
    glm::vec3 m_eye;
    glm::vec3 m_center;
    glm::vec3 m_up;

public:
    explicit Camera(
            const glm::vec3 &rotate = glm::vec3{0.0f},
            const glm::vec3 &translate = glm::vec3{0.0f},
            const glm::vec3 &scale = glm::vec3{0.0f},
            const glm::vec3 &eye = glm::vec3{0.0f, 0.0f, 0.0f},
            const glm::vec3 &center = glm::vec3{0.0f, 0.0f, -1.0f},
            const glm::vec3 &up = glm::vec3{0.0f, 1.0f, 0.0f});

    [[nodiscard]] glm::mat4 getCamera() const;

    [[nodiscard]] const glm::vec3 &getEye() const;

    [[nodiscard]] const glm::vec3 &getCenter() const;

    [[nodiscard]] const glm::vec3 &getUp() const;

    void setEye(const glm::vec3 &mEye);

    void setCenter(const glm::vec3 &mCenter);

    void setUp(const glm::vec3 &mUp);

    void rotate(glm::vec3 eulerAngles) override;

    void translate(glm::vec3 translation) override;

    void lookTo(glm::vec3 direction, glm::vec3 up);

    void render(glm::mat4 cameraMatrix) override;
};


#endif
