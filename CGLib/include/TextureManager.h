//
// Created by ubuntu on 22/12/20.
//

#ifndef CGPROJECT_TEXTUREMANAGER_H
#define CGPROJECT_TEXTUREMANAGER_H

#endif //CGPROJECT_TEXTUREMANAGER_H

#include <unordered_map>
#include "Texture.h"
#include "FilteringTemplate.h"

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
   // Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

class LIB_API TextureManager {
private:
    std::unordered_map<unsigned int, Texture*> m_texture_map{};
    TextureManager();
    FilteringTemplate* m_filtering = nullptr;
public:
    static TextureManager& getInstance();
    void addTexture(unsigned int id, Texture *texture);
    void removeTexture(unsigned int id);
    Texture* getTexture(unsigned int id) const;
    void setFilteringTemplate(FilteringTemplate* filteringTemplate);
    FilteringTemplate *getFilteringTemplate() const;
    void forceMassiveReload() const;
};