//
// Created by kalu on 22/10/20.
//

#ifndef CGPROJECT_CGLIB_INCLUDE_SCENEHIERARCHYTREE_H_
#define CGPROJECT_CGLIB_INCLUDE_SCENEHIERARCHYTREE_H_

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "SceneNode.h"
#include "Camera.h"

class LIB_API SceneGraph {

public:
    static SceneGraph& getInstance();

    [[nodiscard]] std::vector<SceneNode *> getRootNodes() const;

    void setRootNodes(std::vector<SceneNode *> &newRootNodes);

    void addRootNode(SceneNode *rootNode);

    void renderScene(Camera *camera) const;

    [[nodiscard]] SceneNode * findNode(std::string nodeId) const;

    [[nodiscard]] SceneNode *popNode(const std::string& nodeId) const;
private:

    SceneGraph() = default;

  std::vector<SceneNode *> m_rootNodes;
};


#endif
