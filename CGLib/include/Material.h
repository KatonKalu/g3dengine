//
// Created by purpleknight on 11/16/20.
//

#ifndef CGPROJECT_MATERIAL_H
#define CGPROJECT_MATERIAL_H

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif
#else
#define LIB_API
#endif
#pragma endregion DLL Macros Definitions

#include "LuminousObject.h"
#include "cglib.h"
#include "Texture.h"

class LIB_API Material : public LuminousObject {
private:
    float m_shine;
    unsigned int m_orientation;
    Texture *m_texture = nullptr;

public:
    explicit Material(const glm::vec4 &diffuse = glm::vec4(.7f, .7f, .7f, 1.0f),
                      const glm::vec4 &ambient = glm::vec4(.5f, .5f, .5f, 1.0f),
                      const glm::vec4 &specular = glm::vec4(1, 1, 1, 1.0f),
                      const glm::vec4 &emission = glm::vec4(0, 0, 0, 1),
                      float shine = 1,
                      unsigned int orientation = CGLIB_BOTH);

    [[nodiscard]] float getShine() const;

    void setShine(float pShine);

    [[nodiscard]] unsigned int getOrientation() const;

    void setOrientation(unsigned int orientation);

    [[nodiscard]] Texture *getTexture() const;

    void setTexture(Texture *mTexture);

    void apply() override;
};


#endif
