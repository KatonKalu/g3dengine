#pragma once
#include <iostream>
#include "SceneNode.h"
#include "IStrategy.h"

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else
#define LIB_API  
#endif
#pragma endregion DLL Macros Definitions

class LIB_API ParsingStrategy : IStrategy {
protected:
    FILE* m_fp;
    SceneNode *m_parsed;
public:
	virtual void execute() = 0;

	void setInputFile(FILE* fp);

	SceneNode *getResult() const;
};