#pragma once
#include "SceneNode.h"
#include "ParsingStrategy.h"

#pragma region DLL_DEFS
#ifdef _WINDOWS
// Export API:
// Specifies i/o linkage (VC++ spec):
#ifdef CGLIB_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else
#define LIB_API  
#endif
#pragma endregion DLL Macros Definitions


class LIB_API FileParser {
private:
	ParsingStrategy* m_strategy;

public:
	explicit FileParser(ParsingStrategy* strategy = nullptr);
	~FileParser();
	void setStrategy(ParsingStrategy* strategy);
	SceneNode *loadFromFile(const std::string &fileName, bool assetsDirRelative = true);

};