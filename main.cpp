//////////////
// #INCLUDE //
//////////////

//GLM:
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/string_cast.hpp"
#include <glm/gtc/type_ptr.hpp>

// C/C++:
#include <iostream>
#include <SceneGraph.h>
#include <cglib.h>
#include <FileParser.h>
#include <OVOStrategy.h>
#include <CameraManager.h>
#include <TextureManager.h>
#include <AnisotropicFilteringStrategy.h>
#include <LinearFilteringStrategy.h>
#include <NoFilteringStrategy.h>
#include <BilinearFilteringStrategy.h>
#include <TrilinearFilteringStrategy.h>
#include <LightManager.h>
#include <LightBuilder.h>


///////////////
// CALLBACKS //
///////////////
/**
 * This is the main rendering routine automatically invoked by FreeGLUT.
 */

static float openStep = 0.3f;
static float translateStep = 2.0f;
static float totalDelta = 10;
static float currentDelta = 0;
static std::string status;
SceneNode * ray;
SceneNode * rayParent;
Camera camera1;
Camera camera2;
Camera camera3;
Camera camera4;
Camera camera5;
Camera camera6;
Light *dynamic;

void callback_display() {
    SceneNode* leftCap = SceneGraph::getInstance().findNode("obs_left_caps");
    SceneNode* rightCap = SceneGraph::getInstance().findNode("obs_right_caps");

    if(status == "OPENING"){
        leftCap->translate(glm::vec3{openStep, 0, 0});
        rightCap->translate(glm::vec3{-openStep, 0, 0});
        currentDelta+= openStep;

        if(currentDelta >= totalDelta){
            status = "OPEN";
            rayParent->addChild(ray);
        }
    }

    if(status == "CLOSING"){
        leftCap->translate(glm::vec3{-openStep, 0, 0});
        rightCap->translate(glm::vec3{openStep, 0, 0});
        currentDelta-= openStep;

        if(currentDelta <= 0){
            status = "CLOSED";
            rayParent = SceneGraph::getInstance().findNode("ray")->getParent();
            ray = SceneGraph::getInstance().popNode("ray");
        }
    }

    SceneGraph::getInstance().renderScene(CameraManager::getInstance().getActiveCamera());

}

void callback_keyboard(unsigned char key, int mouseX, int mouseY) {

    switch(key){
        case 'e':
            if(status == "CLOSED"){
                status = "OPENING";
            }else if(status == "OPEN"){
                status = "CLOSING";
            }else if(status == "OPENING"){
                status = "CLOSING";
            }else if(status == "CLOSING"){
                status = "OPENING";
            }
            break;

        case '1':
            CameraManager::getInstance().enable(&camera1);
            break;
        case '2':
            CameraManager::getInstance().enable(&camera2);
            break;
        case '3':
            CameraManager::getInstance().enable(&camera3);
            break;
        case '4':
            CameraManager::getInstance().enable(&camera4);
            break;
        case '5':
            CameraManager::getInstance().enable(&camera5);
            break;
        case '6':
            CameraManager::getInstance().enable(&camera6);
            break;

        case 'w':
            dynamic->translate(glm::vec3{0.0f, 0.0f, -translateStep});
            break;
        case 'a':
            dynamic->translate(glm::vec3{-translateStep, 0.0f, 0.0f});
            break;
        case 's':
            dynamic->translate(glm::vec3{0.0f, 0.0f, translateStep});
            break;
        case 'd':
            dynamic->translate(glm::vec3{translateStep, 0.0f, 0.0f});
            break;
        case 'r':
            dynamic->translate(glm::vec3{0.0f, translateStep, 0.0f});
            break;
        case 'f':
            dynamic->translate(glm::vec3{0.0f, -translateStep, 0.0f});
            break;
        default:
            break;
    }

    CGLib::refreshDisplay();

}

void mouse_callback(int mouseX, int mouseY){
    static int prevMouseX = 0;
    static int prevMouseY = 0;

    if(prevMouseX == 0 && prevMouseY == 0){
        prevMouseX = mouseX;
        prevMouseY = mouseY;
        return;
    }

    int deltaX = prevMouseX - mouseX;
    int deltaY = prevMouseY - mouseY;
}

void reshape_callback(int a, int b){


}

/**
 * Application entry point.
 * @param argc number of command-line arguments passed
 * @param argv array containing up to argc passed arguments
 * @return error code (0 on success, error code otherwise)
 */
int main(int argc, char* argv[]) {
    //Init context
    CGLib::setWindowOptions(CGLIB_RGBA | CGLIB_DOUBLE_BUFFER | CGLIB_DEPTH_BUFFER, 1000, 500);
    CGLib::init("CGLib", &argc, argv);

    CGLib::createWindow();

    CGLib::enableLights();
    CGLib::enableFpsCounter();
    CGLib::setBackgroundColor(.2f, .2f, 0.3f, 1.0f);

    CGLib::setDisplayCallback(callback_display);
    CGLib::setKeyboardEventCallback(callback_keyboard);
    CGLib::setWindowReshapeCallback(reshape_callback);

    // Enable Linear + Anisotropic Filtering
    FilteringTemplate filtering{};
    AnisotropicFilteringStrategy anisotropicStrategy{};
    anisotropicStrategy.setMultiplier(8);

    filtering.setAnisotropicStrategy(&anisotropicStrategy);
    filtering.setPerPixelStrategy(new LinearFilteringStrategy());

    TextureManager::getInstance().setFilteringTemplate(&filtering);

    //load scene
    CGLib::setAssetsDirPath("assets");
    std::string fileName = "scene.OVO";
    FileParser fp{};
    fp.setStrategy(new OVOStrategy());

    //create camera
    camera1 = Camera{};
    camera1.translate(glm::vec3{ -100.0f, 160.0f, 302.0f });
    camera1.lookTo(glm::vec3{0.35, -0.4, -1}, glm::vec3{0, 1, 0});

    camera2 = Camera{};
    camera2.translate(glm::vec3{ -100.0f, 160.0f, -300.0f });
    camera2.lookTo(glm::vec3{0.35, -0.4, 1}, glm::vec3{0, 1, 0});

    camera3 = Camera{};
    camera3.translate(glm::vec3{ 100.0f, 160.0f, 300.0f });
    camera3.lookTo(glm::vec3{-0.35, -0.4, -1}, glm::vec3{0, 1, 0});

    camera4 = Camera{};
    camera4.translate(glm::vec3{ 100.0f, 160.0f, -300.0f });
    camera4.lookTo(glm::vec3{-0.35, -0.4, 1}, glm::vec3{0, 1, 0});

    camera5 = Camera{};
    camera5.translate(glm::vec3{ 0.0f, 450.0f, 230.0f });
    camera5.lookTo(glm::vec3{0, -2.5, -1}, glm::vec3{0, 1, 0});

    camera6 = Camera{};
    camera6.translate(glm::vec3{ 80.0f, 88.0f, 40.0f });
    camera6.lookTo(glm::vec3{0, 0, -1}, glm::vec3{0, 1, 0});

    SceneNode* loadedScene = fp.loadFromFile(fileName, true);
    SceneGraph::getInstance().addRootNode(loadedScene);

    //Initial parameters
    status = "CLOSED";
    rayParent = SceneGraph::getInstance().findNode("ray")->getParent();
    ray = SceneGraph::getInstance().popNode("ray");

    dynamic = (Light*)SceneGraph::getInstance().findNode("OmniDirectional");

    CameraManager::getInstance().add(&camera1);
    CameraManager::getInstance().add(&camera2);
    CameraManager::getInstance().add(&camera3);
    CameraManager::getInstance().add(&camera4);
    CameraManager::getInstance().add(&camera5);
    CameraManager::getInstance().add(&camera6);

    CameraManager::getInstance().enable(&camera1);

    CGLib::enterMainLoop();

    std::cout << "[application terminated]" << std::endl;
    return 0;
}
